//
//  MasterViewController.swift
//  SherpanyChallenge
//
//  Created by Denis Svara on 25/03/2017.
//  Copyright © 2017 Denis Svara. All rights reserved.
//

import UIKit
import CoreData

class MasterViewController: UITableViewController
{
	var viewModel: MasterViewModel!
	{
		didSet
		{
			searchController.searchResultsUpdater = viewModel
			tableView.reloadData()
		}
	}
	
	var onPostSelect: ((Post) -> ())?
	let searchController = UISearchController(searchResultsController: nil)
	
	override func viewDidLoad()
	{
		super.viewDidLoad()
		
		self.title = "Challenge Accepted!"
		setupTableView()
		setupSearchController()
		
		self.edgesForExtendedLayout = .init(rawValue: 0)
	}
	
	override func viewDidAppear(_ animated: Bool)
	{
		super.viewDidAppear(animated)
	}

	override func viewWillAppear(_ animated: Bool)
	{
		self.clearsSelectionOnViewWillAppear = self.splitViewController!.isCollapsed
		super.viewWillAppear(animated)
	}

	override func didReceiveMemoryWarning()
	{
		super.didReceiveMemoryWarning()
		// Dispose of any resources that can be recreated.
	}
	
	// MARK: Private
	
	fileprivate func setupTableView()
	{
		tableView.rowHeight = UITableViewAutomaticDimension
		tableView.estimatedRowHeight = 30
		tableView.reloadData()
	}
	
	fileprivate func setupSearchController()
	{
		searchController.dimsBackgroundDuringPresentation = false
		tableView.tableHeaderView = searchController.searchBar
		definesPresentationContext = true
		searchController.searchResultsUpdater = viewModel
	}

	// MARK: UITableViewDataSource
	
	override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
	{
		if viewModel == nil
		{
			return 0
		}
		
		return viewModel.numberOfRows()
	}
	
	override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
	{
		guard let cell = tableView.dequeueReusableCell(withIdentifier: PostCell.reuseIdentifier, for: indexPath) as? PostCell
			else { fatalError("Unexpected cell type at \(indexPath)") }
		
		cell.presenter.post = viewModel.postForIndexPath(indexPath)
		
		return cell
	}
	
	// MARK: UITableViewDelegate
	
	override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool
	{
		return true
	}
	
	override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath)
	{
		if editingStyle == UITableViewCellEditingStyle.delete
		{
			viewModel.deleteAtIndexPath(indexPath)
		}
	}
	
	override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
	{
		onPostSelect?(viewModel.postForIndexPath(indexPath))
	}
}
