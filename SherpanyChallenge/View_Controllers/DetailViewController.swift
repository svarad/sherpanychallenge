//
//  DetailViewController.swift
//  SherpanyChallenge
//
//  Created by Denis Svara on 25/03/2017.
//  Copyright © 2017 Denis Svara. All rights reserved.
//

import UIKit

class DetailViewController: UIViewController
{
	@IBOutlet weak var postTitleLabel: UILabel!
	@IBOutlet weak var postBodyLabel: UILabel!
	@IBOutlet weak var collectionView: UICollectionView!

	var viewModel: DetailViewModel!
	{
		didSet
		{
			updateUI()
		}
	}
	
	override func viewDidLoad()
	{
		super.viewDidLoad()
		let flowLayout = collectionView.collectionViewLayout as! UICollectionViewFlowLayout
		flowLayout.sectionHeadersPinToVisibleBounds = true
		
		postTitleLabel.text = ""
		postTitleLabel.text = ""
	}

	override func didReceiveMemoryWarning()
	{
		super.didReceiveMemoryWarning()
		// Dispose of any resources that can be recreated.
	}
	
	// MARK: Private
	
	fileprivate func updateUI()
	{
		postTitleLabel.text = viewModel.postTitle()
		postTitleLabel.text = viewModel.postBody()
		collectionView.dataSource = self
		collectionView.reloadData()
	}
}

// MARK: UICollectionViewDataSource

extension DetailViewController: UICollectionViewDataSource
{
	func numberOfSections(in collectionView: UICollectionView) -> Int
	{
		return viewModel.numberOfSections()
	}
	
	func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
	{
		return viewModel.numberOfRowsInSection(section: section)
	}
	
	func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
	{
		guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: PhotoCell.reuseIdentifier, for: indexPath) as? PhotoCell else
		{
			fatalError("Unexpected cell type at \(indexPath)")
		}
		
		cell.presenter.imageDownloader = viewModel.imageDownloader
		cell.presenter.photo = viewModel.photoForIndexPath(indexPath)
		
		return cell
	}
	
	func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView
	{
		switch kind
		{
		case UICollectionElementKindSectionHeader:
			guard let headerView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: AlbumHeaderView.reuseIdentifier, for: indexPath) as? AlbumHeaderView else
			{
				fatalError("Unexpected supplementary view type at \(indexPath)")
			}
			
			let section = indexPath.section
			headerView.titleLabel.text = viewModel.albumTitleForSection(section)
			headerView.section = section
			headerView.isOpen = viewModel.isSectionOpen(section)
			headerView.delegate = self
			
			return headerView
			
		default:
			fatalError("Unexpected supplementary view at \(indexPath)")
		}
	}
}

// MARK: AlbumHeaderViewDelegate

extension DetailViewController: AlbumHeaderViewDelegate
{
	func sectionHeaderView(_ view: AlbumHeaderView, sectionTapped section: Int, isOpen: Bool)
	{
		viewModel.setIsSectionOpen(isOpen, section: section)
	}
}

