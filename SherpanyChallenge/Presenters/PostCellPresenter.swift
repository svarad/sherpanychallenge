//
//  PostCellPresenter.swift
//  SherpanyChallenge
//
//  Created by Denis Svara on 27/03/2017.
//  Copyright © 2017 Denis Svara. All rights reserved.
//

import Foundation
import UIKit

protocol PostCellPresenterProtocol: class
{
	func displayTitle(_ title: String?)
	func displayEmail(_ email: String?)
}

class PostCellPresenter
{
	weak var view: PostCellPresenterProtocol?
	var post: Post? = nil
	{
		didSet
		{
			view?.displayTitle(post?.title)
			view?.displayEmail(post?.user?.email)
		}
	}
}
