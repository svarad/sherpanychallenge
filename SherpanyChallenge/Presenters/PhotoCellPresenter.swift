//
//  PhotoCellPresenter.swift
//  SherpanyChallenge
//
//  Created by Denis Svara on 28/03/2017.
//  Copyright © 2017 Denis Svara. All rights reserved.
//

import Foundation
import UIKit
import AlamofireImage

protocol PhotoCellPresenterProtocol: class
{
	func displayTitle(_ title: String?)
	func displayPhoto(_ photo: UIImage)
}

class PhotoCellPresenter
{
	weak var view: PhotoCellPresenterProtocol?
	let placeholder = UIImage(named: "default-placeholder")!
	var photoRequest: RequestReceipt?
	var imageDownloader: ImageDownloader!
	var photo: Photo? = nil
	{
		didSet
		{
			view?.displayTitle(photo?.title)
			view?.displayPhoto(placeholder)
			
			if let request = photoRequest
			{
				imageDownloader?.cancelRequest(with: request)
			}
			
			guard let urlString = photo?.thumbnailUrl,
				let url = URL(string: urlString) else { return }
			let urlRequest = URLRequest(url: url)
			
			photoRequest = imageDownloader.download(urlRequest) { [weak self] response in
				guard let strongSelf = self else { return }
				if let image = response.result.value
				{
					strongSelf.view?.displayPhoto(image)
				}
			}
		}
	}
	
	func prepareForReuse()
	{
		self.photo = nil
	}
}
