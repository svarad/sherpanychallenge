//
//  PhotoCell.swift
//  SherpanyChallenge
//
//  Created by Denis Svara on 28/03/2017.
//  Copyright © 2017 Denis Svara. All rights reserved.
//

import Foundation
import UIKit

class PhotoCell: UICollectionViewCell, ReusableView, PhotoCellPresenterProtocol
{
	@IBOutlet weak var photoImageView: UIImageView!
	@IBOutlet weak var photoTitle: UILabel!
	
	let presenter = PhotoCellPresenter()
	
	override func awakeFromNib()
	{
		super.awakeFromNib()
		presenter.view = self
	}
	
	override func prepareForReuse()
	{
		super.prepareForReuse()
		
		presenter.prepareForReuse()
	}
	
	/// MARK: PhotoCellPresenterProtocol
	
	func displayTitle(_ title: String?)
	{
		photoTitle.text = title
	}
	
	func displayPhoto(_ photo: UIImage)
	{
		photoImageView.image = photo
	}
}
