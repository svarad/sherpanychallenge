//
//  AlbumHeaderView.swift
//  SherpanyChallenge
//
//  Created by Denis Svara on 28/03/2017.
//  Copyright © 2017 Denis Svara. All rights reserved.
//

import Foundation
import UIKit

protocol AlbumHeaderViewDelegate: class
{
	func sectionHeaderView(_ view: AlbumHeaderView, sectionTapped section: Int, isOpen: Bool)
}

class AlbumHeaderView: UICollectionReusableView, ReusableView
{
	@IBOutlet weak var titleLabel: UILabel!
	@IBOutlet weak var albumStateLabel: UILabel!
	
	weak var delegate: AlbumHeaderViewDelegate?
	var section: Int = 0
	var isOpen: Bool = false
	{
		didSet
		{
			albumStateLabel.text = isOpen ? "↓" : "←"
		}
	}
	
	override public func awakeFromNib()
	{
		super.awakeFromNib()
		
		let tapRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.toggleOpen))
		self.addGestureRecognizer(tapRecognizer)
	}
	
	override func prepareForReuse()
	{
		super.prepareForReuse()
		
		isOpen = false
		titleLabel.text = nil
	}
	
	// MARK: Private
	
	func toggleOpen()
	{
		isOpen = !isOpen
		delegate?.sectionHeaderView(self, sectionTapped: section, isOpen: isOpen)
	}
}
