//
//  PostCell.swift
//  SherpanyChallenge
//
//  Created by Denis Svara on 27/03/2017.
//  Copyright © 2017 Denis Svara. All rights reserved.
//

import Foundation
import UIKit

class PostCell: UITableViewCell, ReusableView, PostCellPresenterProtocol
{
	@IBOutlet weak var titleLabel: UILabel!
	@IBOutlet weak var emailLabel: UILabel!
	
	let presenter = PostCellPresenter()
	
	override func awakeFromNib()
	{
		super.awakeFromNib()
		presenter.view = self
	}
	
	/// MARK: PostCellPresenterProtocol
	
	func displayTitle(_ title: String?)
	{
		titleLabel.text = title
	}
	
	func displayEmail(_ email: String?)
	{
		emailLabel.text = email
	}
}
