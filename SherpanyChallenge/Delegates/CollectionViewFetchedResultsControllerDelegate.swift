//
//  CollectionViewFetchedResultsControllerDelegate.swift
//  SherpanyChallenge
//
//  Created by Denis Svara on 28/03/2017.
//  Copyright © 2017 Denis Svara. All rights reserved.
//

import Foundation
import UIKit
import CoreData.NSFetchedResultsController

fileprivate enum Update
{
	case insert(IndexPath)
	case update(IndexPath)
	case move(IndexPath, IndexPath)
	case delete(IndexPath)
}

class CollectionViewFetchedResultsControllerDelegate: NSObject,  NSFetchedResultsControllerDelegate
{
	let collectionView: UICollectionView
	fileprivate var updates: [Update] = []
	fileprivate var sectionsState: [Int : Bool] = [:]
	
	required init(collectionView: UICollectionView)
	{
		self.collectionView = collectionView
	}
	
	func reloadData()
	{
		self.collectionView.reloadData()
	}
	
	fileprivate func processUpdates(_ updates: [Update]?, completion: ((Bool) -> Swift.Void)? = nil)
	{
		guard let updates = updates else { return collectionView.reloadData() }
		collectionView.performBatchUpdates({
			for update in updates
			{
				switch update
				{
				case .insert(let indexPath):
					self.collectionView.insertItems(at: [indexPath])
				case .update(let indexPath):
					self.collectionView.reloadItems(at: [indexPath])
				case .move(let indexPath, let newIndexPath):
					self.collectionView.deleteItems(at: [indexPath])
					self.collectionView.insertItems(at: [newIndexPath])
				case .delete(let indexPath):
					self.collectionView.deleteItems(at: [indexPath])
				}
			}
		}, completion: completion)
	}
	
	// MARK: NSFetchedResultsControllerDelegate
	
	func controllerWillChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>)
	{
//		updates = []
	}
	
	func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange anObject: Any, at indexPath: IndexPath?, for type: NSFetchedResultsChangeType, newIndexPath: IndexPath?)
	{
		switch type {
		case .insert:
			guard let indexPath = newIndexPath else { fatalError("Index path should be not nil") }
			updates.append(.insert(indexPath))
		case .update:
			guard let indexPath = indexPath else { fatalError("Index path should be not nil") }
			updates.append(.update(indexPath))
		case .move:
			guard let indexPath = indexPath else { fatalError("Index path should be not nil") }
			guard let newIndexPath = newIndexPath else { fatalError("New index path should be not nil") }
			updates.append(.move(indexPath, newIndexPath))
		case .delete:
			guard let indexPath = indexPath else { fatalError("Index path should be not nil") }
			updates.append(.delete(indexPath))
		}
	}
	
	func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange sectionInfo: NSFetchedResultsSectionInfo, atSectionIndex sectionIndex: Int, for type: NSFetchedResultsChangeType)
	{
		switch type
		{
		case .insert:
			let set = IndexSet(integer: sectionIndex)
			collectionView.insertSections(set)
		case .delete:
			let set = IndexSet(integer: sectionIndex)
			collectionView.deleteSections(set)
		case .move:
			break
		case .update:
			break
		}
	}
	
	func controllerDidChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>)
	{
		// TODO: Update only objects in open section
	}
}

extension CollectionViewFetchedResultsControllerDelegate
{
	func isSectionOpen(_ section: Int) -> Bool
	{
		if let state = sectionsState[section]
		{
			return state
		}
		
		sectionsState[section] = false
		
		return false
	}
	
	func setIsSectionOpen(_ open: Bool, section: Int, numberOfObjects: Int)
	{
		var rowUpdates: [Update] = []
		
		for i in 0 ..< numberOfObjects
		{
			let indexPath = IndexPath(item: i, section: section)
			if open
			{
				rowUpdates.append(.insert(indexPath))
			}
			else
			{
				rowUpdates.append(.delete(indexPath))
			}
		}
		
		processUpdates(updates) { success in
			self.sectionsState[section] = open
			self.reloadData()
		}
	}
}
