//
//  TableViewFetchedResultsControllerDelegate.swift
//  SherpanyChallenge
//
//  Created by Denis Svara on 27/03/2017.
//  Copyright © 2017 Denis Svara. All rights reserved.
//

import Foundation
import UIKit
import CoreData.NSFetchedResultsController

class TableViewFetchedResultsControllerDelegate: NSObject,  NSFetchedResultsControllerDelegate
{
	let tableView: UITableView
	
	required init(tableView: UITableView)
	{
		self.tableView = tableView
	}
	
	func reloadData()
	{
		self.tableView.reloadData()
	}
	
	func controllerWillChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>)
	{
		tableView.beginUpdates()
	}
	
	func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange anObject: Any, at indexPath: IndexPath?, for type: NSFetchedResultsChangeType, newIndexPath: IndexPath?)
	{
		switch type
		{
		case .insert:
			guard let indexPath = newIndexPath else { fatalError("Index path should be not nil") }
			tableView.insertRows(at: [indexPath], with: .fade)
		case .update:
			guard let indexPath = indexPath else { fatalError("Index path should be not nil") }
			tableView.reloadRows(at: [indexPath], with: .fade)
		case .move:
			guard let indexPath = indexPath else { fatalError("Index path should be not nil") }
			guard let newIndexPath = newIndexPath else { fatalError("New index path should be not nil") }
			tableView.deleteRows(at: [indexPath], with: .fade)
			tableView.insertRows(at: [newIndexPath], with: .fade)
		case .delete:
			guard let indexPath = indexPath else { fatalError("Index path should be not nil") }
			tableView.deleteRows(at: [indexPath], with: .fade)
		}
	}
	
	func controllerDidChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>)
	{
		tableView.endUpdates()
	}
}
