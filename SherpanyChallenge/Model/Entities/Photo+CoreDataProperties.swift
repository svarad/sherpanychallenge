//
//  Photo+CoreDataProperties.swift
//  SherpanyChallenge
//
//  Created by Denis Svara on 27/03/2017.
//  Copyright © 2017 Denis Svara. All rights reserved.
//

import Foundation
import CoreData


extension Photo {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Photo> {
        return NSFetchRequest<Photo>(entityName: "Photo");
    }

    @NSManaged public var identifier: Int64
    @NSManaged public var thumbnailUrl: String?
    @NSManaged public var title: String?
    @NSManaged public var url: String?
    @NSManaged public var album: Album?

}

extension Photo: NSManagedObjectJSONUpdatable
{
	public func update(with json: [String : AnyObject], in context: NSManagedObjectContext) throws
	{
		guard let newId = json["id"] as? Int64,
			let newThumbnailUrl = json["thumbnailUrl"] as? String,
			let newTitle = json["title"] as? String,
			let newUrl = json["url"] as? String,
			let newAlbumId = json["albumId"] as? Int64
			else
		{
			let localizedDescription = NSLocalizedString("Could not interpret data from the server.", comment: "")
			throw NSError(domain: "", code: 999, userInfo: [
				NSLocalizedDescriptionKey: localizedDescription])
		}
		
		identifier = newId
		thumbnailUrl = newThumbnailUrl
		title = newTitle
		url = newUrl
		
		if let album = context.fetchOrInsert(entityName: "Album", with: newAlbumId) as? Album
		{
			album.identifier = newAlbumId
			self.album = album
			album.addToPhotos(self)
		}
	}
}
