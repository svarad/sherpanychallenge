//
//  User+CoreDataProperties.swift
//  SherpanyChallenge
//
//  Created by Denis Svara on 27/03/2017.
//  Copyright © 2017 Denis Svara. All rights reserved.
//

import Foundation
import CoreData


extension User {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<User> {
        return NSFetchRequest<User>(entityName: "User");
    }

    @NSManaged public var email: String?
    @NSManaged public var name: String?
    @NSManaged public var identifier: Int64
    @NSManaged public var username: String?
    @NSManaged public var albums: NSSet?
    @NSManaged public var posts: NSSet?

}

// MARK: Generated accessors for albums
extension User {

    @objc(addAlbumsObject:)
    @NSManaged public func addToAlbums(_ value: Album)

    @objc(removeAlbumsObject:)
    @NSManaged public func removeFromAlbums(_ value: Album)

    @objc(addAlbums:)
    @NSManaged public func addToAlbums(_ values: NSSet)

    @objc(removeAlbums:)
    @NSManaged public func removeFromAlbums(_ values: NSSet)

}

// MARK: Generated accessors for posts
extension User {

    @objc(addPostsObject:)
    @NSManaged public func addToPosts(_ value: Post)

    @objc(removePostsObject:)
    @NSManaged public func removeFromPosts(_ value: Post)

    @objc(addPosts:)
    @NSManaged public func addToPosts(_ values: NSSet)

    @objc(removePosts:)
    @NSManaged public func removeFromPosts(_ values: NSSet)

}

extension User: NSManagedObjectJSONUpdatable
{
	public func update(with json: [String : AnyObject], in context: NSManagedObjectContext) throws
	{
		guard let newId = json["id"] as? Int64,
			let newEmail = json["email"] as? String,
			let newName = json["name"] as? String,
			let newUsername = json["username"] as? String
			else
		{
			let localizedDescription = NSLocalizedString("Could not interpret data from the server.", comment: "")
			throw NSError(domain: "", code: 999, userInfo: [
				NSLocalizedDescriptionKey: localizedDescription])
		}
		
		identifier = newId
		email = newEmail
		name = newName
		username = newUsername
	}
}
