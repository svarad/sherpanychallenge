//
//  Album+CoreDataProperties.swift
//  SherpanyChallenge
//
//  Created by Denis Svara on 27/03/2017.
//  Copyright © 2017 Denis Svara. All rights reserved.
//

import Foundation
import CoreData


extension Album {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Album> {
        return NSFetchRequest<Album>(entityName: "Album");
    }

    @NSManaged public var identifier: Int64
    @NSManaged public var title: String?
    @NSManaged public var photos: NSSet?
    @NSManaged public var users: NSSet?

}

// MARK: Generated accessors for photos
extension Album {

    @objc(addPhotosObject:)
    @NSManaged public func addToPhotos(_ value: Photo)

    @objc(removePhotosObject:)
    @NSManaged public func removeFromPhotos(_ value: Photo)

    @objc(addPhotos:)
    @NSManaged public func addToPhotos(_ values: NSSet)

    @objc(removePhotos:)
    @NSManaged public func removeFromPhotos(_ values: NSSet)

}

// MARK: Generated accessors for users
extension Album {

    @objc(addUsersObject:)
    @NSManaged public func addToUsers(_ value: User)

    @objc(removeUsersObject:)
    @NSManaged public func removeFromUsers(_ value: User)

    @objc(addUsers:)
    @NSManaged public func addToUsers(_ values: NSSet)

    @objc(removeUsers:)
    @NSManaged public func removeFromUsers(_ values: NSSet)

}

extension Album: NSManagedObjectJSONUpdatable
{
	public func update(with json: [String : AnyObject], in context: NSManagedObjectContext) throws
	{
		guard let newId = json["id"] as? Int64,
			let newUserId = json["userId"] as? Int64,
			let newTitle = json["title"] as? String
			else
		{
			let localizedDescription = NSLocalizedString("Could not interpret data from the server.", comment: "")
			throw NSError(domain: "", code: 999, userInfo: [
				NSLocalizedDescriptionKey: localizedDescription])
		}
		
		identifier = newId
		title = newTitle
		
		if let user = context.fetchOrInsert(entityName: "User", with: newUserId) as? User
		{
			user.identifier = newUserId
			addToUsers(user)
			user.addToAlbums(self)
		}
	}
}
