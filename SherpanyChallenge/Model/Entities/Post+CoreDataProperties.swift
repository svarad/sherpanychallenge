//
//  Post+CoreDataProperties.swift
//  SherpanyChallenge
//
//  Created by Denis Svara on 27/03/2017.
//  Copyright © 2017 Denis Svara. All rights reserved.
//

import Foundation
import CoreData


extension Post {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Post> {
        return NSFetchRequest<Post>(entityName: "Post");
    }

    @NSManaged public var body: String?
    @NSManaged public var identifier: Int64
    @NSManaged public var title: String?
    @NSManaged public var user: User?

}

extension Post: NSManagedObjectJSONUpdatable
{
	public func update(with json: [String : AnyObject], in context: NSManagedObjectContext) throws
	{
		guard let newPostId = json["id"] as? Int64,
			let newTitle = json["title"] as? String,
			let newBody = json["body"] as? String,
			let newUserId = json["userId"] as? Int64
			else
		{
			let localizedDescription = NSLocalizedString("Could not interpret data from the server.", comment: "")
			throw NSError(domain: "", code: 999, userInfo: [
				NSLocalizedDescriptionKey: localizedDescription])
		}
		
		identifier = newPostId
		title = newTitle
		body = newBody
		
		if let user = context.fetchOrInsert(entityName: "User", with: newUserId) as? User
		{
			user.identifier = newUserId
			self.user = user
			user.addToPosts(self)
		}
	}
}
