//
//  CoreDataStack.swift
//  SherpanyChallenge
//
//  Created by Denis Svara on 25/03/2017.
//  Copyright © 2017 Denis Svara. All rights reserved.
//

import Foundation
import CoreData

final class CoreDataStack
{
	public func createContainer(completion: @escaping (Error?) -> ())
	{
		persistentContainer.loadPersistentStores(completionHandler: { (storeDescription, error) in
			if let error = error as Error?
			{
				fatalError("Unresolved error \(error)")
			}
			
			DispatchQueue.main.async { completion(error) }
		})
	}

	lazy var persistentContainer: NSPersistentContainer =
	{
		let container = NSPersistentContainer(name: "SherpanyChallenge")
		
		return container
	}()
	
	lazy var viewContext: NSManagedObjectContext = {
		
		let context = self.persistentContainer.viewContext
		context.automaticallyMergesChangesFromParent = true
		try! context.setQueryGenerationFrom(.current)
		context.mergePolicy = NSMergeByPropertyObjectTrumpMergePolicy
		
		return context
	}()

	lazy var backgroundContext: NSManagedObjectContext = {
		let context = self.persistentContainer.newBackgroundContext()
		context.automaticallyMergesChangesFromParent = true
		try! context.setQueryGenerationFrom(.current)
		context.mergePolicy = NSMergeByPropertyObjectTrumpMergePolicy
		context.undoManager = nil
		
		return context
	}()
}
