//
//  RemoteAPI+Albums.swift
//  SherpanyChallenge
//
//  Created by Denis Svara on 27/03/2017.
//  Copyright © 2017 Denis Svara. All rights reserved.
//

import Foundation

extension RemoteAPI
{
	public func fetchAlbums(completion: @escaping([Any]?)->())
	{
		fetch(reqeust: Router.getAlbums(), completion: completion)
	}
}
