//
//  RemoteAPI+Users.swift
//  SherpanyChallenge
//
//  Created by Denis Svara on 27/03/2017.
//  Copyright © 2017 Denis Svara. All rights reserved.
//

import Foundation

extension RemoteAPI
{
	public func fetchUsers(completion: @escaping([Any]?)->())
	{
		fetch(reqeust: Router.getUsers(), completion: completion)
	}
}
