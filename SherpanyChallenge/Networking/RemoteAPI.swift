//
//  RemoteAPI.swift
//  SherpanyChallenge
//
//  Created by Denis Svara on 25/03/2017.
//  Copyright © 2017 Denis Svara. All rights reserved.
//

import Foundation
import Alamofire

public final class RemoteAPI
{
	func fetch(reqeust: URLRequestConvertible, completion:@escaping([Any]?)->())
	{
		Alamofire.request(reqeust).responseJSON { response in
			if let JSON = response.result.value
			{
				completion(JSON as? [Any])
				return
			}
			
			completion(nil)
		}
	}
}

enum Router: URLRequestConvertible
{
	case getPosts()
	case getUsers()
	case getAlbums()
	case getPhotos()
	
	static let baseURLString = "http://jsonplaceholder.typicode.com"
	
	// MARK: URLRequestConvertible
	
	func asURLRequest() throws -> URLRequest
	{
		let result: (path: String, parameters: Parameters?) = {
			switch self
			{
				case .getPosts:
					return ("/posts", nil)
				case .getUsers:
					return ("/users", nil)
				case .getAlbums:
					return ("/albums", nil)
				case .getPhotos:
					return ("/photos", nil)
			}
		}()
		
		let url = try Router.baseURLString.asURL()
		let urlRequest = URLRequest(url: url.appendingPathComponent(result.path))
		
		return try URLEncoding.default.encode(urlRequest, with: result.parameters)
	}
}
