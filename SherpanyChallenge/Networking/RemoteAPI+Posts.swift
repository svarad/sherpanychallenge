//
//  RemoteAPI+Posts.swift
//  SherpanyChallenge
//
//  Created by Denis Svara on 27/03/2017.
//  Copyright © 2017 Denis Svara. All rights reserved.
//

import Foundation

extension RemoteAPI
{
	public func fetchPosts(completion: @escaping([Any]?)->())
	{
		fetch(reqeust: Router.getPosts(), completion: completion)
	}
}
