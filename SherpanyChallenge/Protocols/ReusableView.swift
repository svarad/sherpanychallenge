//
//  ReusableView.swift
//  SherpanyChallenge
//
//  Created by Denis Svara on 27/03/2017.
//  Copyright © 2017 Denis Svara. All rights reserved.
//

import Foundation
import UIKit

protocol ReusableView: class
{
	static var reuseIdentifier: String { get }
}

extension ReusableView where Self: UIView
{
	static var reuseIdentifier: String
	{
		return String(describing: Self.self)
	}
}
