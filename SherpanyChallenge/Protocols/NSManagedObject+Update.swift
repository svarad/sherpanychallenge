//
//  NSManagedObject+Update.swift
//  SherpanyChallenge
//
//  Created by Denis Svara on 27/03/2017.
//  Copyright © 2017 Denis Svara. All rights reserved.
//

import Foundation
import CoreData.NSManagedObject

public protocol NSManagedObjectJSONUpdatable: class
{
	func update(with json: [String: AnyObject], in context: NSManagedObjectContext) throws
}
