//
//  ApplicationStateObserver.swift
//  SherpanyChallenge
//
//  Created by Denis Svara on 27/03/2017.
//  Copyright © 2017 Denis Svara. All rights reserved.
//

import Foundation

/// A protocol for handling application state.
protocol ApplicationStateObserver : class
{
	func applicationDidBecomeActive()
}
