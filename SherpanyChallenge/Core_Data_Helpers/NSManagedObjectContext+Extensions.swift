//
//  NSManagedObjectContext+Extensions.swift
//  SherpanyChallenge
//
//  Created by Denis Svara on 27/03/2017.
//  Copyright © 2017 Denis Svara. All rights reserved.
//

import Foundation
import CoreData

extension NSManagedObjectContext
{
	public func fetchOrInsert<T: NSManagedObject>(entityName: String, with id: Int64) -> T?
	{
		let predicate = NSPredicate(format: "identifier == %d", id)
		// Check if object is already registered in the context
//		if let object = registeredObject(matching: predicate) as? T
//		{
//			print(object)
//			return object
//		}
		
		let request = NSFetchRequest<NSFetchRequestResult>(entityName: entityName)
		request.predicate = predicate
		request.returnsObjectsAsFaults = false
		request.fetchLimit = 1
		
		guard let results = try! self.fetch(request) as? [T] else
		{
			return nil
		}
		
		guard let object = results.first else
		{
			return insert(entityName: entityName)
		}
		
		return object
	}
	
	public func registeredObject<T: NSManagedObject>(matching predicate: NSPredicate) -> T?
	{
		for object in self.registeredObjects where !object.isFault
		{
			guard let result = object as? T, predicate.evaluate(with: result) else { continue }
			return result
		}
		
		return nil
	}

	
	public func insert<T: NSManagedObject>(entityName: String) -> T?
	{
		guard let object = NSEntityDescription.insertNewObject(forEntityName: entityName, into: self) as? T else { return nil }
		
		return object as T
	}
}

extension NSManagedObjectContext
{
	public func saveOrRollback() -> Bool
	{
		do
		{
			try save()
			return true
		}
		catch
		{
			rollback()
			return false
		}
	}
}
