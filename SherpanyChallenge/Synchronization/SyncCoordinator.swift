//
//  SyncCoordinator.swift
//  SherpanyChallenge
//
//  Created by Denis Svara on 25/03/2017.
//  Copyright © 2017 Denis Svara. All rights reserved.
//

import Foundation
import CoreData
import UIKit

/// SyncCoordinator coordinates synchronization with the remote API.
/// It is initialized with a core data stack, remote API and an array of change services. "ChangeService" do the actual job to communicate with the remote and handle data change.

public final class SyncCoordinator
{
	let viewContext: NSManagedObjectContext
	let syncContext: NSManagedObjectContext
	let changeServices: [ChangeService]
	let remoteAPI: RemoteAPI
	
	init(coreDataStack: CoreDataStack, remote: RemoteAPI, services: [ChangeService])
	{
		viewContext = coreDataStack.viewContext
		syncContext = coreDataStack.backgroundContext
		syncContext.name = "SyncCoordinator-SyncQueue"
		syncContext.undoManager = nil
		remoteAPI = remote
		changeServices = services
		setup()
	}
	
	fileprivate func setup()
	{
		setupApplicationNotifications()
		if UIApplication.shared.applicationState == .active
		{
			applicationDidBecomeActive()
		}
	}
	
	fileprivate func fetchData()
	{
		for service in self.changeServices
		{
			service.fetch(in: self)
		}
	}
	
	fileprivate func setupApplicationNotifications()
	{
		NotificationCenter.default.addObserver(forName: .UIApplicationDidBecomeActive, object: nil, queue: nil) { [weak self] note in
			guard let observer = self else { return }
				observer.applicationDidBecomeActive()
		}
		
		NotificationCenter.default.addObserver(forName: .UIApplicationDidEnterBackground, object: nil, queue: nil) { [weak self] note in
			guard let observer = self else { return }
			observer.applicationDidEnterBackground()
		}
	}
}

// MARK: ChangeCoordinatorContext

extension SyncCoordinator: ChangeCoordinatorContext
{
	/// The Context do work on.
	var context: NSManagedObjectContext
	{
		return syncContext
	}
	
	/// Performs the block on the sync context.
	func perform(_ block: @escaping () -> ())
	{
		syncContext.perform(block)
	}
	
	// Saves the context.
	func save()
	{
		guard context.hasChanges else { return }
		
		do
		{
			try context.save()
		}
		catch
		{
			context.rollback()
		}
	}
}

// MARK: ApplicationStateObserver

extension SyncCoordinator: ApplicationStateObserver
{
	func applicationDidBecomeActive()
	{
		fetchData()
	}
	
	func applicationDidEnterBackground()
	{
		viewContext.refreshAllObjects()
		syncContext.refreshAllObjects()
	}
}
