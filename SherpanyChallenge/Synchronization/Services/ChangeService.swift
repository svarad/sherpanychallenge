//
//  ChangeService.swift
//  SherpanyChallenge
//
//  Created by Denis Svara on 25/03/2017.
//  Copyright © 2017 Denis Svara. All rights reserved.
//

import Foundation
import CoreData

// MARK: Change Service

/// A change service performs a specific sync task.
protocol ChangeService
{
	/// The core data entity name.
	var entityName: String { get }
	
	/// Does the initial fetch from the remote.
	func fetch(in context: ChangeCoordinatorContext)
	
	/// Respond to changes of locally inserted or updated objects.
	func processChangedLocalObjects(_ objects: [NSManagedObject], in context: ChangeCoordinatorContext)
}

// MARK: Change Processor Context

/// A protocol that SyncCoordinator conforms to. It is passed to its change services and defined the part they have access to.
protocol ChangeCoordinatorContext: class
{
	/// The managed object context to use.
	var context: NSManagedObjectContext { get }
	
	/// The remote to use for syncing.
	var remoteAPI: RemoteAPI { get }
	
	/// Runs a block on the right queue.
	func perform(_ block: @escaping () -> ())
	
	/// Saves the context.
	func save()
}

extension ChangeService
{
	func insert(_ jsonObjects: [[String: AnyObject]], into context: NSManagedObjectContext, coordinator: ChangeCoordinatorContext)
	{
		// Process records in batches.
		let batchSize = 128
		let count = jsonObjects.count
		
		var numberOfBatches = count / batchSize
		numberOfBatches += count % batchSize > 0 ? 1 : 0
		
		for batchNumber in 0 ..< numberOfBatches
		{
			let batchStart = batchNumber * batchSize
			let batchEnd = batchStart + min(batchSize, count - batchNumber * batchSize)
			let range = batchStart..<batchEnd
			
			let batch = Array(jsonObjects[range])
			importBatch(batch, into: context)
			coordinator.save()
		}
	}
	
	fileprivate func importBatch(_ batch: [[String: AnyObject]], into context: NSManagedObjectContext)
	{
		let jsonIds: [Int] = batch.map { dictionary in
			return dictionary["id"] as! Int
			}.flatMap{$0}
		
		let request = NSFetchRequest<NSFetchRequestResult>(entityName: entityName)
		let predicate = NSPredicate(format: "identifier in %@", argumentArray: [jsonIds])
		request.predicate = predicate
		let sortDescriptor = NSSortDescriptor(key: "identifier", ascending: true)
		request.sortDescriptors = [sortDescriptor]
		let objects = try! context.fetch(request) as! [NSManagedObject]
		
		var result: [Int: NSManagedObject] = [:]
		for object in objects
		{
			let objectId = object.value(forKey: "identifier") as! Int
			result[objectId] = object
		}
		
		for jsonObject in batch
		{
			guard let id = jsonObject["id"] as? Int else { continue }
			var object = result[id]
			if object == nil
			{
				object = NSEntityDescription.insertNewObject(forEntityName: entityName, into: context)
			}
			
			guard let model = object else { continue }
			
			do
			{
				try (model as! NSManagedObjectJSONUpdatable).update(with: jsonObject, in: context)
			}
			catch
			{
				context.delete(model)
			}
		}
	}
}
