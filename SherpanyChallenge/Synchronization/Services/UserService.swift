//
//  UserService.swift
//  SherpanyChallenge
//
//  Created by Denis Svara on 27/03/2017.
//  Copyright © 2017 Denis Svara. All rights reserved.
//

import Foundation
import CoreData

final class UserService: ChangeService
{
	var entityName: String
	{
		return "User"
	}
	
	func fetch(in context: ChangeCoordinatorContext)
	{
		context.remoteAPI.fetchUsers { users in
			guard let users = users else { return }
			context.perform({
				self.insert(users as! [[String: AnyObject]], into: context.context, coordinator: context)
				context.save()
			})
		}
	}
	
	func processChangedLocalObjects(_ objects: [NSManagedObject], in context: ChangeCoordinatorContext)
	{
		
	}
}
