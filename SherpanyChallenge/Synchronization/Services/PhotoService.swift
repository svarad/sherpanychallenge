//
//  PhotoService.swift
//  SherpanyChallenge
//
//  Created by Denis Svara on 27/03/2017.
//  Copyright © 2017 Denis Svara. All rights reserved.
//

import Foundation
import CoreData

final class PhotoService: ChangeService
{
	var entityName: String
	{
		return "Photo"
	}

	func fetch(in context: ChangeCoordinatorContext)
	{
		context.remoteAPI.fetchPhotos { photos in
			guard let photos = photos else { return }
			context.perform({
				self.insert(photos as! [[String: AnyObject]], into: context.context, coordinator: context)
				context.save()
			})
		}
	}
	
	func processChangedLocalObjects(_ objects: [NSManagedObject], in context: ChangeCoordinatorContext)
	{
		
	}
}
