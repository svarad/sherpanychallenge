//
//  AlbumService.swift
//  SherpanyChallenge
//
//  Created by Denis Svara on 27/03/2017.
//  Copyright © 2017 Denis Svara. All rights reserved.
//

import Foundation
import CoreData

final class AlbumService: ChangeService
{
	var entityName: String
	{
		return "Album"
	}

	func fetch(in context: ChangeCoordinatorContext)
	{
		context.remoteAPI.fetchAlbums { albums in
			guard let albums = albums else { return }
			context.perform({
				self.insert(albums as! [[String: AnyObject]], into: context.context, coordinator: context)
				context.save()
			})
		}
	}
	
	func processChangedLocalObjects(_ objects: [NSManagedObject], in context: ChangeCoordinatorContext)
	{
		
	}
}
