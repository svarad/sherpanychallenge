//
//  PostService.swift
//  SherpanyChallenge
//
//  Created by Denis Svara on 26/03/2017.
//  Copyright © 2017 Denis Svara. All rights reserved.
//

import Foundation
import CoreData

final class PostService: ChangeService
{
	var entityName: String
	{
		return "Post"
	}
	
	func fetch(in context: ChangeCoordinatorContext)
	{
		context.remoteAPI.fetchPosts { posts in
			guard let posts = posts else { return }
			context.perform({ 
				self.insert(posts as! [[String: AnyObject]], into: context.context, coordinator: context)
				context.save()
			})
		}
	}
	
	func processChangedLocalObjects(_ objects: [NSManagedObject], in context: ChangeCoordinatorContext)
	{
		
	}
}
