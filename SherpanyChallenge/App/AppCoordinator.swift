//
//  AppCoordinator.swift
//  SherpanyChallenge
//
//  Created by Denis Svara on 25/03/2017.
//  Copyright © 2017 Denis Svara. All rights reserved.
//

import Foundation
import UIKit

class AppCoordinator: UISplitViewControllerDelegate
{
	let stack: CoreDataStack
	var syncCoordinator: SyncCoordinator?
	let remoteAPI = RemoteAPI()
	let splitViewController: UISplitViewController
	
	let masterViewController: MasterViewController
	let detailsViewController: DetailViewController
	
	init(splitViewController: UISplitViewController)
	{
		self.splitViewController = splitViewController
		splitViewController.preferredDisplayMode = .allVisible		
		let detailsNavigationController = splitViewController.viewControllers.last as! UINavigationController
		detailsViewController = detailsNavigationController.topViewController as! DetailViewController
		detailsViewController.navigationItem.leftBarButtonItem = splitViewController.displayModeButtonItem
		
		let masterNavigationController = splitViewController.viewControllers.first as! UINavigationController
		masterViewController = masterNavigationController.topViewController as! MasterViewController
		
		stack = CoreDataStack()
		stack.createContainer { error in
			
			self.start()
			let services: [ChangeService] = [PostService(), UserService(), AlbumService(), PhotoService()]
			self.syncCoordinator = SyncCoordinator(coreDataStack: self.stack, remote: self.remoteAPI, services: services)
		}
	}
	
	fileprivate func start()
	{
		self.splitViewController.delegate = self

		let fetchedResultsDelegate = TableViewFetchedResultsControllerDelegate(tableView: masterViewController.tableView)
		let viewModel = MasterViewModel(managedObjectContext: stack.viewContext, fetchedResultsControllerDelegate: fetchedResultsDelegate)
		masterViewController.viewModel = viewModel
		
		masterViewController.onPostSelect = {[weak self] post in
			guard let strongSelf = self else { return }
			let fetchedResultsDelegate = CollectionViewFetchedResultsControllerDelegate(collectionView: strongSelf.detailsViewController.collectionView)
			let viewModel = DetailViewModel(managedObjectContext: strongSelf.stack.viewContext, fetchedResultsControllerDelegate: fetchedResultsDelegate, post: post)
			strongSelf.detailsViewController.viewModel = viewModel
			strongSelf.splitViewController.showDetailViewController(strongSelf.detailsViewController.navigationController!, sender: nil)
		}
	}
	
	public func splitViewController(_ splitViewController: UISplitViewController, showDetail vc: UIViewController, sender: Any?) -> Bool
	{
		return true
	}

}
