//
//  DetailViewModel.swift
//  SherpanyChallenge
//
//  Created by Denis Svara on 28/03/2017.
//  Copyright © 2017 Denis Svara. All rights reserved.
//

import Foundation
import UIKit
import CoreData
import AlamofireImage

protocol DetailViewModelDelegate: class
{

}

final class DetailViewModel
{
	public let imageDownloader = ImageDownloader()
	
	fileprivate var fetchedResultsController: NSFetchedResultsController<Photo>?
	fileprivate let fetchedResultsControllerDelegate: CollectionViewFetchedResultsControllerDelegate
	fileprivate let managedObjectContext: NSManagedObjectContext
	fileprivate let post: Post
	
	required init(managedObjectContext: NSManagedObjectContext, fetchedResultsControllerDelegate: CollectionViewFetchedResultsControllerDelegate, post: Post)
	{
		self.fetchedResultsControllerDelegate = fetchedResultsControllerDelegate
		self.managedObjectContext = managedObjectContext
		self.post = post
		setupFetchRequest()
	}
	
	func postTitle() -> String
	{
		if let title = post.title
		{
			return title
		}
		
		return ""
	}
	
	func postBody() -> String
	{
		if let body = post.body
		{
			return body
		}
		
		return ""
	}
	
	func numberOfSections() -> Int
	{
		guard let _ = fetchedResultsController else { return 0 }
		
		guard let count = fetchedResultsController!.sections?.count else { return 0 }
		
		return count
	}
	
	func numberOfRowsInSection(section: Int) -> Int
	{
		return numberOfRowsInSection(section: section, ignoreState: false)
	}

	func albumForSection(_ section: Int) -> Album
	{
		let photo = photoForIndexPath(IndexPath(item: 0, section: section))
		let album = photo.album!
		
		return album
	}
	
	func albumTitleForSection(_ section: Int) -> String?
	{
		let photo = photoForIndexPath(IndexPath(item: 0, section: section))
		let album = photo.album!
		
		return album.title
	}
	
	func photoForIndexPath(_ indexPath: IndexPath) -> Photo
	{
		return fetchedResultsController!.object(at: indexPath)
	}

	// MARK: Private
	
	fileprivate func setupFetchRequest()
	{
		let albums = post.user?.albums?.allObjects as! [Album]
		let albumIds: [Int64] = albums.map({$0.identifier})
		let fetchRequest = NSFetchRequest<Photo>(entityName:"Photo")
		let predicate = NSPredicate(format: "album.identifier IN %@", albumIds)
		let sectionSortDescriptor = NSSortDescriptor(key: "album.identifier", ascending: true)
		let photoSortDescriptor = NSSortDescriptor(key: "identifier", ascending: false)
		fetchRequest.sortDescriptors = [sectionSortDescriptor, photoSortDescriptor]
		fetchRequest.predicate = predicate
		let controller = NSFetchedResultsController(fetchRequest: fetchRequest, managedObjectContext: managedObjectContext, sectionNameKeyPath: "album.identifier", cacheName: nil)
		controller.delegate = fetchedResultsControllerDelegate
		fetchedResultsController = controller
		
		do
		{
			try controller.performFetch()
		}
		catch
		{
			let nserror = error as NSError
			fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
		}
	}
	
	fileprivate func numberOfRowsInSection(section: Int, ignoreState: Bool) -> Int
	{
		if ignoreState
		{
			guard let section = fetchedResultsController!.sections?[0] else { return 0 }
			
			return section.numberOfObjects
		}
		
		if isSectionOpen(section)
		{
			guard let section = fetchedResultsController!.sections?[0] else { return 0 }
			
			return section.numberOfObjects
		}
		
		return 0
	}
}

// MARK: Section state handling

extension DetailViewModel
{
	func isSectionOpen(_ section: Int) -> Bool
	{
		return fetchedResultsControllerDelegate.isSectionOpen(section)
	}
	
	func setIsSectionOpen(_ open: Bool, section: Int)
	{
		let numberOfObjects = numberOfRowsInSection(section: section, ignoreState: true)
		fetchedResultsControllerDelegate.setIsSectionOpen(open, section: section, numberOfObjects: numberOfObjects)
	}
}
