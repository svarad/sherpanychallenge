//
//  MasterViewModel.swift
//  SherpanyChallenge
//
//  Created by Denis Svara on 27/03/2017.
//  Copyright © 2017 Denis Svara. All rights reserved.
//

import Foundation
import UIKit
import CoreData

class MasterViewModel: NSObject
{
	fileprivate var fetchedResultsController: NSFetchedResultsController<Post>!
	fileprivate let fetchedResultsControllerDelegate: TableViewFetchedResultsControllerDelegate
	fileprivate let managedObjectContext: NSManagedObjectContext
	
	required init(managedObjectContext: NSManagedObjectContext, fetchedResultsControllerDelegate: TableViewFetchedResultsControllerDelegate)
	{
		self.fetchedResultsControllerDelegate = fetchedResultsControllerDelegate
		self.managedObjectContext = managedObjectContext
		
		super.init()
		
		fetchedResultsController = fetchedResultsControllerForSearchString(nil)
		setupContextNotificationObserving()
	}
	
	func numberOfRows() -> Int
	{
		guard let section = fetchedResultsController.sections?[0] else { return 0 }
		return section.numberOfObjects
	}
	
	func postForIndexPath(_ indexPath: IndexPath) -> Post
	{
		return fetchedResultsController.object(at: indexPath)
	}
	
	func deleteAtIndexPath(_ indexPath: IndexPath)
	{
		let post = postForIndexPath(indexPath)
		managedObjectContext.delete(post)
		let _ = managedObjectContext.saveOrRollback()
	}
	
	// MAKR: Private
	
	fileprivate func setupContextNotificationObserving()
	{
		let notificationCenter = NotificationCenter.default
		notificationCenter.addObserver(forName: .NSManagedObjectContextObjectsDidChange, object: managedObjectContext, queue: nil) { note in
			
			self.fetchedResultsControllerDelegate.reloadData()
		}
	}
	
	fileprivate func defaultFetchRequest() -> NSFetchRequest<Post>
	{
		let fetchRequest = NSFetchRequest<Post>(entityName:"Post")
		fetchRequest.sortDescriptors = [NSSortDescriptor(key: "identifier", ascending: false)]
		fetchRequest.returnsObjectsAsFaults = false
		fetchRequest.fetchLimit = 25;
		
		return fetchRequest
	}
}

extension MasterViewModel: UISearchResultsUpdating
{
	func updateSearchResults(for searchController: UISearchController)
	{
		// TODO: For this case, we'll just reset the fetch reqeust
		if searchController.isActive
		{
			guard let text = searchController.searchBar.text, !text.isEmpty else { return }
			fetchedResultsController = nil
			fetchedResultsController = fetchedResultsControllerForSearchString(text)
		}
		else
		{
			fetchedResultsController = nil
			fetchedResultsController = fetchedResultsControllerForSearchString(nil)
		}

		fetchedResultsControllerDelegate.reloadData()
	}
	
	func fetchedResultsControllerForSearchString(_ string: String? = nil) -> NSFetchedResultsController<Post>
	{
		let fetchRequest = defaultFetchRequest()
		
		if let searchString = string?.lowercased()
		{
			let predicate = NSPredicate(format: "title BEGINSWITH %@", searchString)
			fetchRequest.predicate = predicate
		}
		let controller = NSFetchedResultsController(fetchRequest: fetchRequest, managedObjectContext: managedObjectContext, sectionNameKeyPath: nil, cacheName: nil)
		controller.delegate = fetchedResultsControllerDelegate
		
		do
		{
			try controller.performFetch()
		}
		catch
		{
			let nserror = error as NSError
			fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
		}
		
		return controller
	}
}
