I always start a project from its architecture side. I take my notebook and draw how the architecture should look. After this, I create a project in Xcode and configure it (enable all warnings, treat warnings as errors, supported iOSes). If needed I add CocoaPods. I use git with git flow.

I covered all challenge points but didn't focus on UI that much.
There are also some bugs, occasionally also some crashes, mostly related to synching and not handling all the things.